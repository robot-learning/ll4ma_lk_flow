// #include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>
#include <string>
#include <iostream>
#include <dense_lk.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
  std::cout << "Input images: " << argv[1] << "; " << argv[2] << std::endl;
  cv::Mat input_img0 = cv::imread(argv[1]);
  cv::Mat input_img1 = cv::imread(argv[2]);

  // Test if images read correctly
  if (input_img0.rows < 1 || input_img1.rows < 1 ) return -1;

  // Read optional display threshold from command line
  double thresh = 0.01;
  if (argc > 3) thresh = atof(argv[3]);

  // Construct flow class
  ll4ma_lk_flow::DenseLKFlow lk(5, 4);

  // Run flow computation
  std::vector<cv::Mat> flow_maps = lk(input_img1, input_img0);

  // Display output flow
  cv::imshow("Img0", input_img0);
  cv::imshow("Img1", input_img1);
  ll4ma_lk_flow::displayOpticalFlow(input_img0, flow_maps[0], flow_maps[1], thresh);
  cv::waitKey();

  return 0;
}
