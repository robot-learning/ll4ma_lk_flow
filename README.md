Compute dense Lucas Kanade flow for a pair of images.

Currently builds as a library.

src/test_flow.cpp shows a simple case of running the code.

lk_matlab/README.md discuss running the matlab implementation.
