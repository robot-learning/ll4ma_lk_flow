function [warpI2]=warp(i2,vx,vy)
% warp i2 according to flow field in vx vy
% this is a "backwards" warp: if vx,vy are correct then warpI2==i1
[M,N]=size(i2);
[x,y]=meshgrid(1:N,1:M);
vy(isnan(vy)) = 0;
vx(isnan(vx)) = 0;
warpI3=interp2(x,y,i2,x+vx,y+vy,'*nearest'); % use Matlab interpolation routine
warpI2=interp2(x,y,i2,x+vx,y+vy,'*linear'); % use Matlab interpolation routine
I=find(isnan(warpI2));

if any(any(isnan(warpI3)))
    figure;imshow(warpI3);
    zz = zeros(size(i2));
    oo = ones(size(i2));
    I2=find(isnan(warpI3));    
    zz(I2) = oo(I2);
    figure;imshow(zz);
end

warpI2(I)=warpI3(I);
