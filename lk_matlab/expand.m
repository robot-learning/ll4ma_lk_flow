function image2 = expand(image0,kernel)

% image2 = expand(image);	return an image expanded to double size,
% image2 = expain(image, kernel); specify 1-D kernel with unity sum.

if nargin == 1,
kernel = [1 4 6 4 1]/16; end;		% Default kernel 
[ysize xsize] = size(image0);

kernel = kernel*2;			% kernel sum=2 to account for padding.

image1 = zeros(ysize,2*xsize);		% First double the width
for k = 1:xsize
	image1(:,2*k) = image0(:,k);
end
image1 = filt2(kernel,image1);		% ..and filter horizontally. 
					% filt2 is filter2 with reflection
image2 = zeros(2*ysize,2*xsize);	% Next double the height
for k = 1:ysize
	image2(2*k,:) = image1(k,:);
end
image2 = filt2(kernel',image2);		% ..and filter vertically.

