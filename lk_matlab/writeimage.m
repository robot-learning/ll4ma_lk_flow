function f(x,fname,datatype)
% Syntax: writeimage(X,fname,datatype)
%
% Writes the matrix X to disk as a binary file. fname is the
% output filename (including path) and must be a Matlab
% string array (e.g., fname = '/users/mylogin/images/frame0.dat' )
% datatype is also a Matlab string specifying the precision used
% in the value of each pixel:
%
%  datatype = 'uchar'   for 8-bit pixels (unsigned char)
%  datatype = 'float'   for float arrays
%
% See also FREAD, FWRITE (for description of precisions/datatypes)
%
% NOTE: This routine writes the matrix in row-order. This is
%       different from Matlab's fwrite() function. But matches
%       the format of most binary image file formats.

fid = fopen(fname,'wb');
if fid==-1
	disp(['ERROR: fopen failed on filename: ' fname])
	return;
end;

% write the transpose of the matrix so as to make it row-order
fwrite(fid,x',datatype);

fclose(fid);
 
