function [g]=shiftleft(f1)

zn=size(f1);
n=zn(:,1);

g=f1;

for i=1:(n-2);
  g(:,i)=g(:,i+1);
end
