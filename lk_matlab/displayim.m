function []=displayIm(im,scale,autoScale)
% display a gray scale image with control over size
% usually matlab resizes every image to be the size of the figure 
% this creates blocky images which look very ugly
% here we resize the figure to be a multiple (scale) of the size of the image
% scale should be an integer.
% use displayIm(im) to display the image as is
% displayIm(im,2) to display the image at twice the size
% displayIm(im,2,1) to display the image at twice the size and autoscale

if (nargin==1)
  scale=1;autoScale=0;
end
if (nargin==2)
  autoScale=0;
end
clf;
P=get(gcf,'Position');
[M,N]=size(im);
P(3:4)=scale*[N M];
set(gcf,'Position',P);
colormap(gray(256));
axes('Position',[0  0 1 1]);
if (autoScale==1)
  mn=min(min(im));
  mx=max(max(im));
  im=(im-mn)*255/(mx-mn);
end
image(im);
axis('image');
axis('off');
