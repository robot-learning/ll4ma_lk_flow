function [sm]=smooth(i2,n)
% function [sm] = smooth(image,n)
% reduce and expand n times.

if nargin == 1,
  n=1;
end

sm=i2;
for l=1:n
  sm=reduce(sm);
end

for l=1:n
  sm=expand(sm);
end
