function [g]=gauss(n,m,cx,cy,sigmax,sigmay)

g=ones(n,m);
tot=0;

for i=1:n
  for j=1:m
    g(i,j) = exp(-sqrt((i-cx)*(i-cx)/(sigmax) + (j-cy)*(j-cy)/sigmay));
    tot = tot + g(i,j);
  end
end

g=g/tot;
