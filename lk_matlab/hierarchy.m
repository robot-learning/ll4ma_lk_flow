function [Dx,Dy]=hierarchy(f1,f2)

dims = size(f1);
n    = dims(:,1);
m    = dims(:,2);
t    = floor(log(min(n,m))/log(2))-4

for l=t:-1:0
    if (l==t)
        Dx = zeros(n,m);
        for i=1:t
            Dx = reduce(Dx);
        end;
        Dy = Dx;
    else
        Dx = expand(Dx);
        Dy = expand(Dy);
    end
    size(Dx)
    g1 = f1;
    for i=1:l
        g1 = reduce(g1);
    end;
    
    g2 = f2;
    for i=1:l
        g2 = reduce(g2);
    end;
    
    W = warp(g1,Dx,Dy);
    
    [vx,vy] = optic(W,g2);
    
    Dx = Dx + vx;
    Dy = Dy + vy;
    Dx = smooth(Dx);
    Dy = smooth(Dy);
    
end

