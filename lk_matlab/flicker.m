function [m]=flicker(im1,im2)
% [m]=flicker(im1,im2)
% constructs a gray movie of flickering the two images
s1 = size(im1); s2 = size(im2);
if not(all (s1 == s2)) error('Unmatched sizes.'); end

mov = zeros(s1(1),s1(2),1,2);
mov(:,:,1,1) = im1;
mov(:,:,1,2) = im2;
m=immovie(mov*255,gray);
