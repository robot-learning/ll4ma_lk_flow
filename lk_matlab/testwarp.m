function [m]=testwarp(im1,im2,dx,dy)
% testwarp(im1,im2,dx,dy)
w = warp(im2,dx,dy);
m=flicker(im1,w);
movie(m,15,2);
m;
