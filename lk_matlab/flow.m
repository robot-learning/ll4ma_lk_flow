 So if you want to debug your code run it on:
[i1,i2]=texSquare(32,0,1,0,2); 
after calculating vx,vy in your program display them with displayFlow
and you should see downward arrows.
Likewise try:
[i1,i2]=texSquare(32,1,0,0,2);
and you should now see rightward arrows.

  Yair

==========================================================================
function []=displayFlow(vx,vy,jump);
% display a flow field using Matlab's quiver command
% convention: positive vx -> rightward velocity
%             positive vy -> downward velocity.
% input: vx,vy: matrices containing the x and y components of flow field
%        jump:  an integer specifying density of quiver plot.
%               jump=1 -> an arrow will be displayed for every pixel
%               jump=2 -> an arrow will be displayed for every second pixel
%               etc.
[maxI,maxJ]=size(vx);
vx=vx(1:jump:maxI,1:jump:maxJ);
vy=vy(1:jump:maxI,1:jump:maxJ);
quiver(1:jump:maxJ,(maxI):(-jump):1,vx,-vy,1);
axis square;
=========================================================================



