Old Matlab code to perform Lucas-Kanade optical flow, written by Yair Weiss while a student at MIT.

To run on two images i1, i2:


```
#!matlab

  [vx, vy] = optic(i1,i2);

```

To then display the resulting flow field:


```
#!matlab

  displayflow(vx,vy)
```