function []=histogram(im)

if (nargin==1)
  kernel=[1 4 6 4 1] / 16; binsize=10;
end
if (nargin==2)
  binsize=10;
end

gauss1=reduce(im,kernel);
gauss2=reduce(gauss1,kernel);
gauss3=reduce(gauss2,kernel);
gauss4=reduce(gauss3,kernel);
gauss5=reduce(gauss4,kernel);

g1=gauss1;
g2=gauss2;
g3=gauss3;
g4=gauss4;
g5=gauss5;

g1=expand(g1,kernel);

g2=expand(g2,kernel);
g2=expand(g2,kernel);

g3=expand(g3,kernel);
g3=expand(g3,kernel);
g3=expand(g3,kernel);

g4=expand(g4,kernel);
g4=expand(g4,kernel);
g4=expand(g4,kernel);
g4=expand(g4,kernel);

g5=expand(g5,kernel);
g5=expand(g5,kernel);
g5=expand(g5,kernel);
g5=expand(g5,kernel);
g5=expand(g5,kernel);

laplace0=im-g1;
laplace1=g1-g2;
laplace2=g2-g3;
laplace3=g3-g4;
laplace4=g4-g5;
laplace5=g5;


lquant0=(round(laplace0/binsize)+0.5)*binsize;
lquant1=(round(laplace1/binsize)+0.5)*binsize;
lquant2=(round(laplace2/binsize)+0.5)*binsize;
lquant3=(round(laplace3/binsize)+0.5)*binsize;
lquant4=(round(laplace4/binsize)+0.5)*binsize;
lquant5=(round(laplace5/binsize)+0.5)*binsize;

approx=laplace0+laplace1+laplace2+laplace3+laplace4+laplace5;

approx2=lquant0+lquant1+lquant2+lquant3+lquant4+lquant5;

numPixels0=size(im);
numPixels1=numPixels0/2;
numPixels2=numPixels1/2;
numPixels3=numPixels2/2;
numPixels4=numPixels3/2;
numPixels5=numPixels4/2;

[N,Y]=imhist(lquant0);
H0=-sum(N*ln2(N))/numPixels0;



bitrate= (H0*numPixels0 + H1*numPixels1 + H2*numPixels2  + H3*numPixels3 + H4*numPixels4 + H5*numPixels5)    / (numPixels0 + numPixels1 + numPixels2 + numPixels3 + numPixels4 + numPixels5)




displayIm(im,1,1)
% print -deps gauss0.eps
  pause
displayIm(gauss1,2,1)
% print -deps gauss1.eps
  pause
displayIm(gauss2,4,1)
% print -deps gauss2.eps
  pause
displayIm(gauss3,8,1)
% print -deps gauss3.eps
  pause
displayIm(gauss4,16,1)
% print -deps gauss4.eps
  pause
displayIm(gauss5,32,1)
% print -deps gauss5.eps
  pause


displayIm(im,1,1)
% print -deps g0.eps
  pause
displayIm(g1,1,1)
% print -deps g1.eps
  pause
displayIm(g2,1,1)
% print -deps g2.eps
  pause
displayIm(g3,1,1)
% print -deps g3.eps
  pause
displayIm(g4,1,1)
% print -deps g4.eps
  pause
displayIm(g5,1,1)
% print -deps g5.eps


displayIm(laplace0,1,1)
% print -deps laplace0.eps
  pause
displayIm(laplace1,1,1)
% print -deps laplace1.eps
  pause
displayIm(laplace2,1,1)
% print -deps laplace2.eps
  pause
displayIm(laplace3,1,1)
% print -deps laplace3.eps
  pause
displayIm(laplace4,1,1)
% print -deps laplace4.eps
  pause
displayIm(laplace5,1,1)
% print -deps laplace5.eps
  pause


displayIm(lquant0,1,1)
% print -deps lquant0.eps
  pause
displayIm(lquant1,1,1)
% print -deps lquant1.eps
  pause
displayIm(lquant2,1,1)
% print -deps lquant2.eps
  pause
displayIm(lquant3,1,1)
% print -deps lquant3.eps
  pause
displayIm(lquant4,1,1)
% print -deps lquant4.eps
  pause
displayIm(lquant5,1,1)
% print -deps lquant5.eps
  pause

displayIm(approx,1,1)
  pause

displayIm(approx2,1,1)



