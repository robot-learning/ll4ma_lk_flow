function x = f(fname,Nrow,Ncol,datatype)
% Syntax:  X = loadimage(fname,Nrows,Ncols,datatype)
%
% Loads an Nrows-by-Ncols raw (no header) binary image into the
% matrix X. fname is the filename (including path) and must be a Matlab
% string array (e.g., fname = '/users/mylogin/images/frame0.dat'; )
% datatype is also a Matlab string specifying the datatype:
%
%  datatype = 'uchar'   for 8-bit pixels (unsigned char)
%  datatype = 'float'   for float arrays
%
% See also FREAD (for description of various datatypes)
%
% NOTE: This functions assumes the image data is written in row-order

fid = fopen(fname);
if fid==-1
	disp(['ERROR: fopen failed on filename: ' fname])
	return;
end;

% Since Matlab's fread() column-concatenates the data we have to read
% an Ncol-by-Nrow matrix first then transpose the result so as to have
% row-concatenation (which is more standard)

x = fread(fid,[Ncol Nrow],datatype)';  % NOTE: Matlab col-concatenates!
fclose(fid);
 
