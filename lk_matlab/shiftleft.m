function [g]=shiftleft(f1,by)

zn=size(f1);
n=zn(:,2);

g=f1;

for i=1:n;
  if (i<(n-by)) 
    g(:,i)=g(:,i+by);
  else
    g(:,i)=g(:,n);
  end
end
