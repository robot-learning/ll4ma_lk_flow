function [vx,vy]=optic(f1,f2)

dims = size(f1);
n    = dims(:,1);
m    = dims(:,2);

vx = ones(n,m);
vy = ones(n,m);

fl = shiftleft(f2,1);
fr = shiftright(f2,1);
fu = (shiftleft(f2',1))';
fd = (shiftright(f2',1))';

fx = (fl - fr) / 2.0;
fy = (fu - fd) / 2.0;
ft = f2 - f1;

g  = gauss(5,5,3,3,10,10);
%g  = gauss(50,50,30,30,100,100);

fxx =  fx .* fx;
fxy =  fx .* fy;
fyy =  fy .* fy;

fxt =  fx .* ft;
fyt =  fy .* ft;

W11 = conv2(fxx,g,'same');
W22 = conv2(fyy,g,'same');
W12 = conv2(fxy,g,'same');

G1  = conv2(fxt,g,'same');
G2  = conv2(fyt,g,'same');

for i=1:n
  for j=1:m
    W = [W11(i,j) W12(i,j); W12(i,j) W22(i,j)];
    [V,D] = eig(W);
    if (max(D(1,1),D(2,2)) < 0.00001)
      vx(i,j)=0;
      vy(i,j)=0;
    elseif (min(D(1,1),D(2,2)) < (max(D(1,1),D(2,2))/10))
      G = [G1(i,j) G2(i,j)]';
      if (D(1,1)>D(2,2))
        v = V(:,1)'*G*V(:,1)/D(1,1);
      else
        v = V(:,2)'*G*V(:,2)/D(2,2);
      end
      vx(i,j)=v(1);
      vy(i,j)=v(2);
    else
      G = [G1(i,j) G2(i,j)]';
      v = V(:,1)'*G*V(:,1)/D(1,1) + V(:,2)'*G*V(:,2)/D(2,2);
      vx(i,j)=v(1);
      vy(i,j)=v(2);
    end
  end
end


