function image2 = reduce(image0,kernel)

% Reduce: for building Gaussian, Laplacian pyramids. 1-D separable kernels.

% imnew = reduce(im0)		Reduce w/default kernel: [1 4 6 4 1]/16

% imnew = reduce(im0,kern)	Reduce with kern; sums to unity.

if nargin == 1,
kernel = [1 4 6 4 1]/16; end;		% Default kernel 

[ysize xsize] = size(image0);

ysize2 = round(ysize/2);
xsize2 = round(xsize/2);

image0 = filt2(kernel,image0);		% Filter horizontally. 
					% filt2 is filter2 with reflection.
image1 = zeros(ysize,xsize2);		% Decimate horizontally
for k = 1:xsize2
	image1(:,k) = image0(:,k*2);
end

image1 = filt2(kernel',image1);		% Filter vertically.
image2 = zeros(ysize2,xsize2);		% Decimate vertically.
for k = 1:ysize2
	image2(k,:) = image1(k*2,:);
end
