function [g]=shiftright(f1,by)

zn=size(f1);
n=zn(:,2);

g=f1;

for i=n:-1:1;
  if (i>by) 
    g(:,i)=g(:,i-by);
  else
    g(:,i)=g(:,1);
  end
end
